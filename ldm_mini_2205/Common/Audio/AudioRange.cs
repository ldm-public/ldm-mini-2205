﻿using Nez;

namespace ldm_mini_2205 {
	public struct AudioRange {
		float _min;
		public float Minimum { get => _min; set => _min = Mathf.Clamp(value, -1f, 1f); }

		float _max;
		public float Maximum { get => _max; set => _max = Mathf.Clamp(value, -1f, 1f); }

		public AudioRange(float min, float max) {
			_min = min;
			_max = max;
		}

		public float Randomize() => Minimum + Random.NextFloat(Maximum - Minimum);
	}
}
