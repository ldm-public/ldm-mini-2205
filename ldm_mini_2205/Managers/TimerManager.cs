using System;
using System.Collections.Generic;

using Nez;

namespace ldm_mini_2205.Managers {
	public class TimerManager : GlobalManager {
		List<Timer> _timers = new List<Timer>();

		public override void Update() {
			for (var i = _timers.Count - 1; i >= 0; i--) {
				// tick our timer. if it returns true it is done so we remove it
				if (_timers[i].Tick()) {
					_timers[i].Unload();
					_timers.RemoveAt(i);
				}
			}
		}

		public ITimer Schedule(float timeInSeconds, Action<ITimer> onTime) => Schedule(timeInSeconds, false, null, onTime);

		public ITimer Schedule(float timeInSeconds, bool repeats, Action<ITimer> onTime) => Schedule(timeInSeconds, repeats, null, onTime);

		public ITimer Schedule(float timeInSeconds, bool repeats, object context, Action<ITimer> onTime) {
			var timer = new Timer();
			timer.Initialize(timeInSeconds, repeats, context, onTime);
			_timers.Add(timer);

			return timer;
		}
	}

	public class Timer : ITimer {
		public object Context { get; set; }

		float _timeInSeconds;
		bool _repeats;
		Action<ITimer> _onTime;
		bool _isDone;
		float _elapsedTime;

		public void Stop() => _isDone = true;

		public void Reset() => _elapsedTime = 0f;

		public T GetContext<T>() => (T)Context;

		internal bool Tick() {
			if (
				!_isDone &&
				_elapsedTime > _timeInSeconds
			) {
				_elapsedTime -= _timeInSeconds;
				_onTime(this);

				if (!_isDone && !_repeats) {
					_isDone = true;
				}
			}

			_elapsedTime += Time.DeltaTime;

			return _isDone;
		}

		internal void Initialize(float timeInSeconds, bool repeats, object context, Action<ITimer> onTime) {
			_timeInSeconds = timeInSeconds;
			_repeats = repeats;
			Context = context;
			_onTime = onTime;
		}

		internal void Unload() {
			Context = null;
			_onTime = null;
		}
	}
}