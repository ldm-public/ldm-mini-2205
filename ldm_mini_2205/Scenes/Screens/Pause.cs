using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using Nez;
using Nez.UI;

using ldm_mini_2205.Scenes.ScreenManagers;

namespace ldm_mini_2205.Scenes.Screens {
	public class Pause : GameScreen {
		VirtualButton _unPauseButton;

		public Pause(ScreenManager screenManager)
			: base(screenManager, @"Pause", false)
		{
			_unPauseButton = new VirtualButton();
			_unPauseButton.Nodes.Add(new VirtualButton.KeyboardKey(Keys.F1));
			_unPauseButton.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Escape));
			_unPauseButton.Nodes.Add(new VirtualButton.GamePadButton(0, Buttons.Start));
		}

		public override void HandleInput() {
			if (_unPauseButton.IsPressed) {
				Exiting = true;
			}
		}

		protected override void LoadContent() {
			base.LoadContent();

			CreateEntity(@"pause-backdrop").SetPosition(new Vector2(Screen.Width / 2, Screen.Height / 2))
				.AddComponent(new PrototypeSpriteRenderer(Screen.Width, Screen.Height) {
					Color = new Color(0f, 0f, 0f, 0f),
					RenderLayer = RenderLayers.PauseScreen
				});

			_canvas = CreateEntity($"{_name.ToLower()}-ui").AddComponent(new UICanvas() {
				RenderLayer = RenderLayers.PauseScreen,
				Color = Color.Transparent,
				IsFullScreen = true
			});

			_table = _canvas.Stage.AddElement(new Table());
			_table.SetFillParent(true).Row().Center().Top().Expand(true, false);
			_table.Add(new Label(_name, new LabelStyle() { FontColor = Color.White, FontScale = 16f })).SetAlign(Align.Top);
		}

		public override void Update(float deltaTime, bool otherScreenHasFocus, bool coveredByOtherScreen) {
			base.Update(deltaTime, otherScreenHasFocus, coveredByOtherScreen);

			Color fade = Color.Black;
			if (
				_screenState == ScreenState.TransitionOn ||
				_screenState == ScreenState.TransitionOff
			) {
				fade = new Color(0f, 0f, 0f, TransitionAlpha);
			} else if (_screenState == ScreenState.Hidden) {
				fade = Color.Transparent;
			}

			for (int i = 0; i < _entities.Length; i++) {
				if (_entities[i].TryGetComponent(out RenderableComponent renderable)) {
					renderable.Color = fade;
				}
			}
		}
	}
}

