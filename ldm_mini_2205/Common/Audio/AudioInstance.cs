﻿using Microsoft.Xna.Framework.Audio;

namespace ldm_mini_2205 {
	public struct AudioInstance {
		public string FileName { get; set; }

		public SoundEffect Sound { get; set; }

		public AudioRange? Pan { get; set; }
		public AudioRange? Pitch { get; set; }
		public AudioRange? Volume { get; set; }

		public AudioInstance(SoundEffect soundEffect) {
			Sound = soundEffect;

			FileName = @"";
			Pan = null;
			Pitch = null;
			Volume = null;
		}

		public SoundEffectInstance Play() {
			var sound = Sound.CreateInstance();

			sound.Volume = Volume.HasValue ? Volume.Value.Randomize() : 1f;
			sound.Pitch = Pitch.HasValue ? Pitch.Value.Randomize() : 0f;
			sound.Pan = Pan.HasValue ? Pan.Value.Randomize() : 0f;
			sound.Play();

			return sound;
		}
	}
}
