# LDM-Mini-2205

Alright. Mays project is going to be something a little different again. Instead of it being a game or gameplay type thing I'm going to try do something a little more practical. I want to create a stackable screen manager in Nez that's similar to the [XNA Game State Sample](https://github.com/CartBlanche/MonoGame-Samples/tree/master/GameStateManagement "Game State Management").

---

## Running Task List

## Known Bugs

---

## May 16 2022
Implemented MonoSound.FNA library and made a "Global Manager" for the sound. I created "local" global managers (I know) for each of the "Game Screens" that get created so that everything could be paused. In order to access the per Game Screen managers I made an extension function for Entity that will grab the local version if it implements EntityGs or the true global version if it doesn't.

( Sound effects used in this sample were obtained from https://www.zapsplat.com )

## May 13 2022
We did it. Go us. I'm not entirely sure how usable this would be in an actual game, but as a sample it works really well. Wanted to give a quick shoutout to the guys over in the Nez discord for being *insanely* helpful.

## May 02 2022
Created project.