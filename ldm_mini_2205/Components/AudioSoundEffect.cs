﻿using System.Collections.Generic;

using Nez;

using ldm_mini_2205.Managers;
using ldm_mini_2205.Scenes.ScreenManagers;

namespace ldm_mini_2205.Components {
	class AudioSoundEffect : Component {
		List<string> _required;

		AudioManager _audioManager;

		public AudioSoundEffect(params string[] soundEffects) {
			_required = ListPool<string>.Obtain();
			_required.AddRange(soundEffects);
		}

		public override void OnAddedToEntity() {
			_audioManager = Entity.GetGlobalManager<AudioManager>();

			foreach (string filename in _required) {
				_audioManager.PreloadSound(filename);
			}

			ListPool<string>.Free(_required);
		}

		public void Play(string filename, AudioRange? volume = null, AudioRange? pitch = null, AudioRange? pan = null) => _audioManager.PlaySound(filename, volume, pitch, pan);

		public void Play(string filename, float volume, AudioRange? pitch = null, AudioRange? pan = null) => _audioManager.PlaySound(filename, new AudioRange(volume, volume), pitch, pan);
	}
}
