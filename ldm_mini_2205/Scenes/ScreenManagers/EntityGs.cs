using Nez;

using ldm_mini_2205.Managers;

namespace ldm_mini_2205.Scenes.ScreenManagers {
	public class EntityGs : Entity {
		protected GameScreen _gameScreen;
		public virtual GameScreen GameScreen { get => _gameScreen; protected internal set => _gameScreen = value; }

		protected bool _paused = false;
		public virtual bool Paused { get => _paused; protected internal set => _paused = value; }

		public EntityGs() : base() { }

		public EntityGs(string name) : base(name) { }

		public override void Update() {
			if (!Paused) {
				base.Update();
			}
		}
	}

	public static class EntityGsExt {
		public static T GetGlobalManager<T>(this Entity entity) where T : GlobalManager {
			if (entity is EntityGs entityGs) {
				return entityGs.GameScreen.GetGlobalManager<T>();
			}

			return Core.GetGlobalManager<T>();
		}
	}
}