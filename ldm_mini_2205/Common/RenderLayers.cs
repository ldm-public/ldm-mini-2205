namespace ldm_mini_2205 {
	public static class RenderLayers {
		public static readonly int PauseScreen = 5;
		public static readonly int Overlay = 10;
		public static readonly int Gameplay = 15;
	}
}

