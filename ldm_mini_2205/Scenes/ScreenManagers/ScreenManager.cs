using Nez;

namespace ldm_mini_2205.Scenes.ScreenManagers {
	public class ScreenManager : Scene {
		FastList<GameScreen> screens = new FastList<GameScreen>();
		FastList<GameScreen> screensToUpdate = new FastList<GameScreen>();

		public ScreenManager() : base() { }

		public void AddScreen(GameScreen screen, ScreenState screenState = ScreenState.TransitionOn, bool autostart = true) {
			screen.ScreenManager = this;
			screen.Exiting = false;

			// If we have a graphics device, tell the screen to load content.
			screen.Initialize();

			screen.SetEnabled(autostart);
			if (screenState != screen.ScreenState) {
				screen.ScreenState = screenState;
				screen.TransitionPosition = screenState == ScreenState.TransitionOn ? 1f : 0f;
			}

			screens.Add(screen);

			screen.OnAdd();
		}

		public void RemoveScreen(GameScreen screen, bool unload = false) {
			screen.OnRemove();

			// If we have a graphics device, tell the screen to unload content.
			if (screen.Initialized && unload) {
				screen.UnloadContent();
			}

			screens.Remove(screen);
			screensToUpdate.Remove(screen);

			screen.SetEnabled(false);
		}

		public override void Update() {
			// Make a copy of the master screen list, to avoid confusion if the process of updating one screen adds or removes others.
			screensToUpdate.Clear();
			for (var i = 0; i < screens.Length; i++) {
				screensToUpdate.Add(screens[i]);
			}

			bool otherScreenHasFocus = false;
			bool coveredByOtherScreen = false;

			// Loop as long as there are screens waiting to be updated.
			while (screensToUpdate.Length > 0) {
				// Pop the topmost screen off the waiting list.
				GameScreen screen = screensToUpdate[screensToUpdate.Length - 1];

				screensToUpdate.RemoveAt(screensToUpdate.Length - 1);

				// Update the screen.
				screen.Update(Time.DeltaTime, otherScreenHasFocus, coveredByOtherScreen);

				if (
					screen.ScreenState == ScreenState.TransitionOn ||
					screen.ScreenState == ScreenState.Active
				) {
					// If this is the first active screen we came across, give it a chance to handle input.
					if (!otherScreenHasFocus) {
						if (screen.ScreenState == ScreenState.Active) {
							screen.HandleInput();
						}

						otherScreenHasFocus = true;
					}

					// If this is an active non-popup, inform any subsequent screens that they are covered by it.
					if (!screen.Overlay) {
						coveredByOtherScreen = true;
					}
				}
			}

			base.Update();
		}

		public override void Unload() {
			base.Unload();

			// Tell each of the screens to unload their content.
			for (var i = 0; i < screens.Length; i++) {
				screens[i].UnloadContent();
			}
		}
	}
}

