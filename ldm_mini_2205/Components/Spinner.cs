using Microsoft.Xna.Framework;

using Nez;

namespace ldm_mini_2205.Components {
	public class Spinner : Component, IUpdatable {
		public override void OnAddedToEntity() {
			base.OnAddedToEntity();

			// Transform
			// 	.Tween(@"Rotation", MathHelper.TwoPi, 1f)
			// 	.SetLoops(Nez.Tweens.LoopType.PingPong, -1)
			// 	.SetEaseType(Nez.Tweens.EaseType.Linear)
			// 	.Start();
		}

		public override void OnRemovedFromEntity() {
			base.OnRemovedFromEntity();
		}

		public void Update() {
			Transform.Rotation += (MathHelper.TwoPi / 60f);
		}
	}
}