using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using Nez;
using Nez.UI;

using ldm_mini_2205.Scenes.ScreenManagers;

namespace ldm_mini_2205.Scenes.Screens {
	public class Overlay : GameScreen {
		VirtualButton _closeButton;

		public Overlay(ScreenManager screenManager)
			: base(screenManager, @"Overlay", false)
		{
			_closeButton = new VirtualButton();
			_closeButton.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Enter));
			_closeButton.Nodes.Add(new VirtualButton.GamePadButton(0, Buttons.Back));
		}

		public override void HandleInput() {
			if (_closeButton.IsPressed) {
				Exiting = true;
			}
		}

		protected override void LoadContent() {
			base.LoadContent();

			CreateEntity(@"pause-backdrop").SetPosition(new Vector2(Screen.Width / 2, Screen.Height / 2))
				.AddComponent(new PrototypeSpriteRenderer(640, 240) {
					Color = new Color(0f, 0f, 1f, 0.875f),
					RenderLayer = RenderLayers.Overlay
				});

			_canvas = CreateEntity($"{_name.ToLower()}-ui").AddComponent(new UICanvas() {
				RenderLayer = RenderLayers.Overlay,
				Color = Color.Transparent,
				IsFullScreen = true
			});

			_table = _canvas.Stage.AddElement(new Table());
			_table.SetFillParent(true).Row().Center().Top().Expand(true, false);
			_table.Add(new Label(_name, new LabelStyle() { FontColor = Color.Green, FontScale = 16f })).SetAlign(Align.Top);
		}

		public override void Update(float deltaTime, bool otherScreenHasFocus, bool coveredByOtherScreen) {
			base.Update(deltaTime, otherScreenHasFocus, coveredByOtherScreen);

			Color fade = new Color(0f, 0f, 1f, 0.875f);
			if (
				_screenState == ScreenState.TransitionOn ||
				_screenState == ScreenState.TransitionOff
			) {
				fade = new Color(0f, 0f, 1f, 0.875f * TransitionAlpha);
			} else if (_screenState == ScreenState.Hidden) {
				fade = Color.Transparent;
			}

			for (int i = 0; i < _entities.Length; i++) {
				if (_entities[i].TryGetComponent(out RenderableComponent renderable)) {
					renderable.Color = fade;
				}
			}
		}
	}
}