using ldm_mini_2205.Managers;

using Microsoft.Xna.Framework;

using Nez;
using Nez.Systems;
using Nez.Tweens;
using Nez.UI;

using System;
using System.Collections;
using System.Collections.Generic;

namespace ldm_mini_2205.Scenes.ScreenManagers {
	public enum ScreenState {
		TransitionOn,
		Active,
		TransitionOff,
		Hidden,
	}

	public abstract class GameScreen {
		bool _otherScreenHasFocus;
		public bool Active =>
			!_otherScreenHasFocus && (
				_screenState == ScreenState.TransitionOn ||
				_screenState == ScreenState.Active
			);

		bool _exiting = false;
		public bool Exiting { get => _exiting; internal protected set => _exiting = value; }

		protected FastList<GlobalManager> _globalManagers = new FastList<GlobalManager>();
		public IReadOnlyList<GlobalManager> GlobalManagers => _globalManagers.Buffer;

		protected bool _initialized = false;
		public bool Initialized { get => _initialized; internal protected set => _initialized = value; }

		bool _overlay = false;
		public bool Overlay { get => _overlay; internal protected set => _overlay = value; }

		protected ScreenManager _screenManager;
		public ScreenManager ScreenManager { get => _screenManager; internal protected set => _screenManager = value; }

		protected ScreenState _screenState = ScreenState.TransitionOn;
		public ScreenState ScreenState { get => _screenState; internal protected set =>  _screenState = value; }

		protected float _transitionOnTime = 0.5f;
		public float TransitionOnTime { get => _transitionOnTime; set => _transitionOnTime = value; }

		protected float _transitionOffTime = 0.5f;
		public float TransitionOffTime{ get => _transitionOffTime; set => _transitionOffTime = value; }

		protected float _transitionPosition = 1f;
		public float TransitionPosition { get => _transitionPosition; internal protected set => _transitionPosition = value; }

		public float TransitionAlpha => 1f - TransitionPosition;

		protected string _name = @"";
		protected bool _enabled = false;
		protected bool _focused = false;

		protected FastList<Entity> _entities = new FastList<Entity>();

		protected UICanvas _canvas;
		protected Table _table;

		protected CoroutineManager _coroutineManager;
		protected TimerManager _timerManager;
		protected AudioManager _audioManager;

		public GameScreen(ScreenManager screenManager, string name, bool preload = true) : base() {
			_screenManager = screenManager;
			_name = name;

			_coroutineManager = new CoroutineManager();
			_globalManagers.Add(_coroutineManager);

			_timerManager = new TimerManager();
			_globalManagers.Add(_timerManager);

			_audioManager = new AudioManager();
			_globalManagers.Add(_audioManager);

			if (preload) {
				Initialize();
			}
		}

		~GameScreen() => UnloadContent();

		public virtual EntityGs CreateEntity(string name) {
			var entity = _screenManager.AddEntity(new EntityGs(name) {
				Enabled = false,
				GameScreen = this,
				Paused = true,
			});

			_entities.Add(entity);

			return entity;
		}

		public T GetGlobalManager<T>() where T : GlobalManager {
			for (var i = 0; i < _globalManagers.Length; i++) {
				if (GlobalManagers[i] is T globalManager) {
					return globalManager;
				}
			}

			return Core.GetGlobalManager<T>();
		}

		public abstract void HandleInput();

		public void Initialize() {
			if (!_initialized) {
				LoadContent();
			}

			_initialized = true;
		}

		protected virtual void LoadContent() { }

		public virtual void OnAdd() { }

		public virtual void OnRemove() { }

		public virtual void Pause() {
			for (var i = 0; i < _globalManagers.Length; i++) {
				_globalManagers[i].SetEnabled(false);
			}

			for (var i = 0; i < _entities.Length; i++) {
				if (_entities[i] is EntityGs entGS) {
					entGS.Paused = true;
				}
			}

			foreach (var tweener in TweenManager.ActiveTweens) {
				if (tweener is ITweenControl tweenControl) {
					var target = tweenControl.GetTargetObject();
					if (
						target is Entity entity &&
						_entities.Contains(entity) ||
						target is Component component &&
						_entities.Contains(component.Entity) ||
						target is Transform transform &&
						_entities.Contains(transform.Entity)
					) {
						tweenControl.Pause();
					}
				}
			}
		}

		public virtual void Resume() {
			for (var i = 0; i < _globalManagers.Length; i++) {
				_globalManagers[i].SetEnabled(true);
			}

			for (var i = 0; i < _entities.Length; i++) {
				if (_entities[i] is EntityGs entGS) {
					entGS.Paused = false;
				}
			}

			foreach (var tweener in TweenManager.ActiveTweens) {
				if (tweener is ITweenControl tweenControl) {
					var target = tweenControl.GetTargetObject();
					if (
						target is Entity entity &&
						_entities.Contains(entity) ||
						target is Component component &&
						_entities.Contains(component.Entity) ||
						target is Transform transform &&
						_entities.Contains(transform.Entity)
					) {
						tweenControl.Resume();
					}
				}
			}
		}

		public ITimer Schedule(float timeInSeconds, Action<ITimer> onTime) => Schedule(timeInSeconds, false, null, onTime);

		public ITimer Schedule(float timeInSeconds, bool repeats, Action<ITimer> onTime) => Schedule(timeInSeconds, repeats, null, onTime);

		public ITimer Schedule(float timeInSeconds, bool repeats, object context, Action<ITimer> onTime) => _timerManager.Schedule(timeInSeconds, repeats, context, onTime);

		public virtual GameScreen SetEnabled(bool enabled, bool togglePause = true) {
			if (_enabled != enabled) {
				for (var i = 0; i < _entities.Length; i++) {
					_entities[i].SetEnabled(enabled);
				}

				if (togglePause) {
					if (enabled) {
						Resume();
					} else if (!enabled) {
						Pause();
					}
				}
			}

			_enabled = enabled;

			return this;
		}

		public ICoroutine StartCoroutine(IEnumerator enumerator) => _coroutineManager.StartCoroutine(enumerator);

		public virtual void Update(float deltaTime, bool otherScreenHasFocus, bool coveredByOtherScreen) {
			_otherScreenHasFocus = otherScreenHasFocus;

			if (Exiting) {
				// If the screen is going away to die, it should transition off.
				_screenState = ScreenState.TransitionOff;

				if (!UpdateTransition(deltaTime, _transitionOffTime, 1)) {
					// When the transition finishes, remove the screen.
					ScreenManager.RemoveScreen(this);
				}
			} else if (coveredByOtherScreen) {
				Pause();

				// If the screen is covered by another, it should transition off.
				if (UpdateTransition(deltaTime, _transitionOffTime, 1)) {
					// Still busy transitioning.
					_screenState = ScreenState.TransitionOff;
				} else {
					// Transition finished!
					_screenState = ScreenState.Hidden;

					SetEnabled(false);

					_focused = false;
				}
			} else {
				// Otherwise the screen should transition on and become active.
				if (UpdateTransition(deltaTime, _transitionOnTime, -1)) {
					// Still busy transitioning.
					_screenState = ScreenState.TransitionOn;

					if (!_enabled) {
						SetEnabled(true, false);
					}
				} else {
					// Transition finished!
					_screenState = ScreenState.Active;

					if (_focused && otherScreenHasFocus) {
						Pause();

						_focused = false;
					} else if (!_focused && !otherScreenHasFocus) {
						Resume();

						_focused = true;
					}
				}
			}

			for (var i = 0; i < _globalManagers.Length; i++) {
				if (_globalManagers[i].Enabled) {
					_globalManagers[i].Update();
				}
			}

			//	Entity Clean-up
			for (var i = 0; i < _entities.Length;) {
				if (_entities[i].IsDestroyed) {
#if DEBUG
					System.Console.WriteLine("{0} was removed from {1}", _entities[i].Name, _name);
#endif
					_entities.RemoveAt(i);
					continue;
				}

				i++;
			}
		}

		bool UpdateTransition(float deltaTime, float time, int direction) {
			// How much should we move by?
			float transitionDelta;

			if (time <= 0f) {
				transitionDelta = 1;
			} else {
				transitionDelta = Mathf.Clamp(deltaTime / time, 0, 1f);
			}

			// Update the transition position.
			_transitionPosition += transitionDelta * direction;

			// Did we reach the end of the transition?
			if (
				direction < 0 && _transitionPosition <= 0 ||
				direction > 0 && _transitionPosition >= 1
			) {
				_transitionPosition = MathHelper.Clamp(_transitionPosition, 0, 1);

				return false;
			}

			// Otherwise we are still busy transitioning.
			return true;
		}

		public virtual void UnloadContent() {
			if (_initialized) {
				_initialized = false;
			}
		}
	}
}

