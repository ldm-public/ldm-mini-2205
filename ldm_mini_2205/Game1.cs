using MonoSound;

using Nez;
using Nez.ImGuiTools;

using ldm_mini_2205.Managers;
using ldm_mini_2205.Scenes.Screens;
using ldm_mini_2205.Scenes.ScreenManagers;

namespace ldm_mini_2205 {
	class Game1 : Core {
		public Game1() : base() {
			// uncomment this line for scaled pixel art games
			// System.Environment.SetEnvironmentVariable("FNA_OPENGL_BACKBUFFER_SCALE_NEAREST", "1");
		}

		override protected void Initialize() {
			base.Initialize();

			var screenManager = new ScreenManager();
			Scene = screenManager;

			screenManager.AddScreen(new GamePlay(screenManager), ScreenState.Active);

#if DEBUG
			System.Diagnostics.Debug.Listeners.Add(new System.Diagnostics.TextWriterTraceListener(System.Console.Out));

			// optionally render Nez in an ImGui window
			Core.RegisterGlobalManager(new ImGuiManager());
			Core.RegisterGlobalManager(new AudioManager());

			// optionally load up ImGui DLL if not using the above setup so that its command gets loaded in the DebugConsole
			//System.Reflection.Assembly.Load("Nez.ImGui")
#endif
		}

		protected override void LoadContent() {
			base.LoadContent();
			MonoSoundManager.Init();
		}

		protected override void UnloadContent() {
			base.UnloadContent();
			MonoSoundManager.DeInit();
		}
	}
}
