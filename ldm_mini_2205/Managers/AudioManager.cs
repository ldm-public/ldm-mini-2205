﻿using System.Collections.Generic;

using Microsoft.Xna.Framework.Audio;

using MonoSound;

using Nez;

namespace ldm_mini_2205.Managers {
	public class AudioManager : GlobalManager {
		Dictionary<string, SoundEffect> _soundEffectsLookup = new Dictionary<string, SoundEffect>();

		List<AudioInstance> _queuedSoundEffects = new List<AudioInstance>();
		List<SoundEffectInstance> _activeSoundEffects = new List<SoundEffectInstance>();

		SoundEffectInstance _music;

		bool _paused = false;

		public override void OnDisabled() {
			base.OnDisabled();
			Pause();
		}

		public override void OnEnabled() {
			base.OnEnabled();
			Resume();
		}

		public void Pause() {
			_paused = true;

			foreach (var sfx in _activeSoundEffects) {
				sfx.Pause();
			}

			if (_music != null) {
				_music.Pause();
			}
		}

		public void PlayMusic(string filename, float volume, bool fade = true, float time = 0.3f) {
			if (_music != null) {
				if (fade) {
					_music.Tween("Volume", 0f, time).SetCompletionHandler((_) => {
						_music.Stop();
						_music = MonoSoundManager.GetStreamedSound($@"Content/Audio/music/{filename}.mp3", true);
						_music.Volume = 0f;
						_music.Play();
						_music.Tween("Volume", volume, time);
					}).Start();
				} else {
					_music.Play();
				}
			} else {
				_music = MonoSoundManager.GetStreamedSound($@"Content/Audio/music/{filename}.mp3", true);
				_music.Volume = volume;
				_music.Play();
			}
		}

		public void PlaySound(string filename, AudioRange? volume = null, AudioRange? pitch = null, AudioRange? pan = null) {
			if (!_soundEffectsLookup.ContainsKey(filename)) {
				PreloadSound(filename);
			}

			if (_queuedSoundEffects.FindIndex((effect) => effect.FileName == filename) == -1) {
				_queuedSoundEffects.Add(new AudioInstance(_soundEffectsLookup[filename]) {
					FileName = filename,
					Volume = volume,
					Pitch = pitch,
					Pan = pan
				});
			}
		}

		public void PreloadSound(string filename) {
			if (!_soundEffectsLookup.ContainsKey(filename)) {
				_soundEffectsLookup.Add(filename, MonoSoundManager.GetEffect($@"./Content/Audio/{filename}.wav"));
			}
		}

		public void PreloadSounds(params string[] filenames) {
			foreach (var filename in filenames) {
				PreloadSound(filename);
			}
		}

		public void Resume() {
			_paused = false;

			foreach (var sfx in _activeSoundEffects) {
				sfx.Resume();
			}

			if (_music != null) {
				_music.Resume();
			}
		}

		public void Stop(bool fade = false, float time = 0.3f) {
			_paused = false;

			StopSound(fade, time);
			StopMusic(fade, time);
		}

		public void StopMusic(bool fade = false, float time = 0.3f) {
			if (fade) {
				_music.Tween("Volume", 0f, time).SetCompletionHandler((_) => {
					StopMusic();
				}).Start();
			} else {
				_music.Stop(true);
				_music.Dispose();
			}
		}

		public void StopSound(bool fade = false, float time = 0.3f) {
			for (int i = 0; i < _activeSoundEffects.Count; i++) {
				var effect = _activeSoundEffects[i];

				if (fade) {
					effect.Tween("Volume", 0f, time).SetCompletionHandler((_) => {
						effect.Stop();
						effect.Dispose();
					}).Start();
				} else {
					effect.Stop(true);
					effect.Dispose();
				}
			}

			_activeSoundEffects.Clear();
			_queuedSoundEffects.Clear();
		}

		public override void Update() {
			if (!_paused && Enabled) {
				for (int i = 0; i < _activeSoundEffects.Count;) {
					var effect = _activeSoundEffects[i];
					if (effect.State == SoundState.Stopped) {
						_activeSoundEffects.RemoveAt(i);

						effect.Dispose();

						continue;
					}

					i++;
				}

				for (int i = 0; i < _queuedSoundEffects.Count; i++) {
					var effect = _queuedSoundEffects[i].Play();
					if (effect != null) {
						_activeSoundEffects.Add(effect);
					}
				}

				_queuedSoundEffects.Clear();
			}
		}
	}
}
