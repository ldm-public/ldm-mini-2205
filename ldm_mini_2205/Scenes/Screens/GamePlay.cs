using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using Nez;
using Nez.UI;

using ldm_mini_2205.Components;
using ldm_mini_2205.Managers;
using ldm_mini_2205.Scenes.ScreenManagers;

namespace ldm_mini_2205.Scenes.Screens {
	public class GamePlay : GameScreen {
		Pause _pauseScreen;
		Overlay _overlayScreen;

		VirtualButton _pauseButton;
		VirtualButton _overlayButton;

		public GamePlay(ScreenManager screenManager)
			: base(screenManager, @"Gameplay", true)
		{
			_pauseScreen = new Pause(screenManager) {
				TransitionOnTime = 0.2f,
				TransitionOffTime = 0.2f,
			};

			_overlayScreen = new Overlay(screenManager) {
				TransitionOnTime = 0.2f,
				TransitionOffTime = 0.1f,
				Overlay = true,
			};

			_pauseButton = new VirtualButton();
			_pauseButton.Nodes.Add(new VirtualButton.KeyboardKey(Keys.F1));
			_pauseButton.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Escape));
			_pauseButton.Nodes.Add(new VirtualButton.GamePadButton(0, Buttons.Start));

			_overlayButton = new VirtualButton();
			_overlayButton.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Enter));
			_overlayButton.Nodes.Add(new VirtualButton.GamePadButton(0, Buttons.Back));
		}

		public override void HandleInput() {
			if (_pauseButton.IsPressed) {
				ScreenManager.AddScreen(_pauseScreen);
			} else if (_overlayButton.IsPressed) {
				ScreenManager.AddScreen(_overlayScreen);
			}
		}

		protected override void LoadContent() {
			base.LoadContent();

			_canvas = CreateEntity($"{_name.ToLower()}-ui").AddComponent(new UICanvas() {
				RenderLayer = RenderLayers.Gameplay,
				IsFullScreen = true
			});

			_table = _canvas.Stage.AddElement(new Table());
			_table.SetFillParent(true).Row().Center().Top().Expand(true, false);
			_table.Add(new Label(_name, new LabelStyle() { FontColor = Color.Red, FontScale = 16f })).SetAlign(Align.Top);

			CreateEntity(@"pawn").SetPosition(new Vector2(150, 150))
				.AddComponent<Spinner>()
				.AddComponent(new PrototypeSpriteRenderer(75, 75) {
					RenderLayer = RenderLayers.Gameplay,
				}).AddComponent(new AudioSoundEffect(@"drone") { });

			var pawn = _screenManager.FindEntity(@"pawn");

			pawn
				.Tween("Scale", new Vector2(0.5f), 0.3f)
				.SetEaseType(Nez.Tweens.EaseType.Linear)
				.SetLoops(Nez.Tweens.LoopType.PingPong, -1, 0f)
				.Start();

			pawn.GetComponent<RenderableComponent>()
				.TweenColorTo(Color.Black, 0.3f)
				.SetEaseType(Nez.Tweens.EaseType.Linear)
				.SetLoops(Nez.Tweens.LoopType.PingPong, -1, 0f)
				.Start();

			GetGlobalManager<TimerManager>().Schedule(1f, false, (timer) => {
				pawn.GetComponent<AudioSoundEffect>().Play(@"drone", 1f);
			});

			var destroyMe = CreateEntity(@"destroy-me");
			destroyMe.Destroy();
		}

		public override void UnloadContent(){
			base.UnloadContent();
		}

		public override void Update(float deltaTime, bool otherScreenHasFocus, bool coveredByOtherScreen) {
			base.Update(deltaTime, otherScreenHasFocus, coveredByOtherScreen);
		}
	}
}